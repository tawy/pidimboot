###
# Script by Ezechiel
# Date : 2019/05/26
# Description :  PiDimBoot button
###

#Write Text on screen
def fontLabel(surface, screen, policies, label, size, pos_x, pos_y, rgb_code):
	font=surface.font.Font(policies, size)	 
	screen.blit(font.render(str(label), 1, (rgb_code)), (pos_x, pos_y))

#Create Button witch Image and Text or Not
def createButton(surface, screen, button, policies, label, size, pos_x, pos_y, rgb_code, decal_x=70, decal_y=15): 
	img_button = surface.image.load(button).convert_alpha()
	screen.blit(img_button,(pos_x-decal_x, pos_y-decal_y))

	fontLabel(surface, screen, policies, label, size, pos_x, pos_y, rgb_code)
	
#Create back button
def backButton(surface, screen, button, color):
	img_back = surface.image.load(button).convert_alpha()
	screen.blit(img_back, (5, 5))
	surface.draw.rect(screen, color, (3, 3, 42, 37), 3)
	
	
#Create previous button 
def prevButton(surface,screen, button):
	img_prev = surface.image.load(button).convert_alpha()
	screen.blit(img_prev, (5, 140))
	
#Create next  button 
def nextButton(surface,screen, button):
	img_next = surface.image.load(button).convert_alpha()
	screen.blit(img_next, (435, 140))